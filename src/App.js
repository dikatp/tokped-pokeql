import React, { Fragment } from 'react';
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';
import { Switch, Route } from "react-router-dom";

/** @jsxImportSource @emotion/react */
import { jsx } from '@emotion/react';

import { OwnPokemonProvider } from "./contexts/OwnPokemonContext";

import { PokemonList } from './pages/PokemonList';
import { PokemonDetail } from './pages/PokemonDetail';
import { OwnedPokemonList } from './pages/OwnedPokemonList';

const client = new ApolloClient({
  uri: 'https://graphql-pokeapi.vercel.app/api/graphql',
  cache: new InMemoryCache()
});

function App() {
  return (
    <Fragment>
      <ApolloProvider client={client}>
        <OwnPokemonProvider>
          <Switch>
            <Route exact path="/pokemon/:name" component={PokemonDetail} />
            <Route exact path="/owned_pokemon" component={OwnedPokemonList} />
            <Route exact path="/" component={PokemonList} />
          </Switch>
        </OwnPokemonProvider>
      </ApolloProvider>
    </Fragment>
  );
}

export default App;
