import React, { Fragment, useContext, useState, useEffect } from 'react';

/** @jsxImportSource @emotion/react */
import { jsx } from '@emotion/react';
import styled from '@emotion/styled';

import { OwnPokemonContext } from "../contexts/OwnPokemonContext";

import Colors from '../assets/themes/colors'

const Backdrop = styled.div`
  width: 100%;
  height: 100%;
  position: fixed;
  z-index: 100;
  left: 0;
  top: 0;
  background-color: rgba(0,0,0,0.65);
  padding: 0px 20px;
`;

const BaseModal = styled.div`
  display: block;
  position: fixed;
  top: 50%;
  left: 50%;
  background-color: ${props =>
    props.backgroundCOlor ?? 'rgba(0,0,0,0.55)'};
  border-radius: 12px;
  text-align: center;
  z-index: 128;
  transition: opacity 0.3s ease 0s;
  padding: 30px 10px;
  max-width: 400px;
  transform: ${props =>
    props.show ? 'translate(-50%, -50%)' : 'translateY(100vh)'};
  opacity: ${props =>
    props.show ? 1 : 0};
`;

const BodyText = styled.div`
  font-size: 16;
  font-family: 'Barlow Semi Condensed', sans-serif;
  color: ${props =>
    props.color ?? 'white'};
  text-align: center;
`;

const ModalTitle = styled.div`
  border-bottom: 1px solid lightgrey;
  padding-bottom: 20px;
  margin-bottom: 20px;
  text-align: center;
  font-size: 18px;
  font-family: 'Barlow Semi Condensed', sans-serif;
  font-weight: bold;
`;

const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 20px;
`;

const NicknameInput = styled.input`
  width: 50%;
  margin: auto;
  outline: none;
`;

const ErrorMessage = styled.span`
    width: 50%;
    font-size: 12px;
    font-family: 'Barlow Semi Condensed', sans-serif;
    color: red;
    margin: 5px auto 0px auto;
    text-align: justify;
`;

const ButtonContainer = styled.div`
    width: 100%;
    display: flex;
    justify-content: space-evenly;
    margin-top: 20px;
`;

const SaveButton = styled.button`
    width: 20%;
    font-size: 12px;
    font-family: 'Barlow Semi Condensed', sans-serif;
    font-color: 'black';
    background-color: ${Colors.keppel};
    border: 1px solid ${Colors.mirage};
    border-radius: 4px;
    padding: 5px 0;
    outline: none;
    &:hover:enabled {
      background-color: ${Colors.swansDown};
    }
`;

function BackdropComponent(props) {
  return(
    props.show ? <Backdrop onClick={() => props.closeModal ? props.closeModal(!props.show) : ''}/> : null
  )
}

function NotifModalComponent(props) {
  return (
    <Fragment>
      <BackdropComponent show={props.show} closeModal={props.closeModal} />
      <BaseModal show={props.show} >
        <BodyText>{props.message}</BodyText>
      </BaseModal>
    </Fragment>
  )
}

function CatchModalComponent(props) {
  const { state, dispatch } = useContext(OwnPokemonContext);
  const [nickname, setNickname] = useState('');
  const [disableButton, setDisableButton] = useState(false);
  let ownedPokemons = state.ownPokemons;

  const savePokemon = (obj) => {
    obj.nickname = nickname
    obj.id = state.lastId

    if(nickname !== ''){
      setNickname('')
    }
    
    dispatch({
      type: 'CATCH_POKEMON',
      payload: {
        data: obj,
      }
    })

    props.confirmHandler()
  }

  useEffect(() => {
    let isSubscribed = true
    
    let result = ownedPokemons.filter(obj => {
      return obj.nickname.toLowerCase() === nickname.toLowerCase() && obj.nickname !== ''
    })

    if(isSubscribed){
      result. length > 0 ? setDisableButton(true) : setDisableButton(false)
    }

    return () => (isSubscribed = false)
  }, [nickname]);

  return (
    <Fragment>
      <BackdropComponent show={props.show} />
      <BaseModal backgroundCOlor={Colors.swansDown} show={props.show} >
        <ModalTitle>Pokemon Nickname</ModalTitle>
        <BodyText color={Colors.mirage}>Congratulations! You successfully captured a pokemon. You can give your new pokemon a nickname</BodyText>
        <InputContainer>
          <NicknameInput type='text' onChange={e => setNickname(e.target.value) } value={nickname} />
          {disableButton ? <ErrorMessage>Nickname already taken! Please try another one.</ErrorMessage> :''}
        </InputContainer>
        <ButtonContainer>
          <SaveButton disabled={disableButton} onClick={() => savePokemon(props.pokemon)}>Save</SaveButton>
        </ButtonContainer>
      </BaseModal>
    </Fragment>
  )
}

function ReleaseModal(props) {
  const { state, dispatch } = useContext(OwnPokemonContext);
  const releasePokemon = () => {
    dispatch({
      type: 'DELETE_POKEMON',
      payload: {
        data: props.pokemon,
      }
    })

    props.confirmHandler()
  }

  return(
    <Fragment>
      <BackdropComponent show={props.show} />
      <BaseModal backgroundCOlor={Colors.swansDown} show={props.show}>
        <ModalTitle>Release "{props.pokemon ? (props.pokemon.nickname || props.pokemon.name).toUpperCase() : ''}"</ModalTitle>
        <BodyText color={Colors.mirage}>Are you sure to release pokemon?</BodyText>
        <ButtonContainer>
          <SaveButton onClick={() => props.closeModal(false)}>Cancel</SaveButton>
          <SaveButton onClick={() => releasePokemon()}>Release</SaveButton>
        </ButtonContainer>
      </BaseModal>
    </Fragment>
  )
}

export { NotifModalComponent, CatchModalComponent, ReleaseModal }