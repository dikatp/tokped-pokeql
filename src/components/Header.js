import React, { useState } from 'react';

/** @jsxImportSource @emotion/react */
import { jsx, css } from '@emotion/react';
import styled from '@emotion/styled';

import useDocumentScrollThrottled from '../helper/useDocumentScrollThrottled';
import Colors from '../assets/themes/colors'

const defaultHeader = css({
  width: '100%',
  boxShadow: 'rgba(12,12,13,0.05) 0 1px 2px 0',
  backgroundColor: Colors.keppel,
  position: 'fixed',
  zIndex: '40',
  transition: 'background-color 0.24s ease-in',
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  height: '50px',
  maxWidth: '500px',
  padding: '0px 20px',
  boxSizing: 'border-box',
  color: Colors.twilight,
  fontSize: '20px',
})

const transparent = css({
  width: '100%',
  background: 'linear-gradient(to bottom, #58595b 0%, rgba(0,0,0,0) 100%)',
  position: 'fixed',
  zIndex: '40',
  transition: 'background-color 0.24s ease-in',
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  height: '50px',
  maxWidth: '500px',
  padding: '0px 20px',
  boxSizing: 'border-box',
  color: Colors.twilight,
  fontSize: '20px',
})

const HeaderTitle = styled.div`
  text-align: center;
  flex: 1;
`;

export function Header(props) {
  const [transparentHeader, setTransparentHeader] = useState(false);

  const MINIMUM_SCROLL = 40;
  const TIMEOUT_DELAY = 100;

  useDocumentScrollThrottled(callbackData => {
    const { previousScrollTop, currentScrollTop } = callbackData;
    const isScrolledDown = previousScrollTop < currentScrollTop;
    const isMinimumScrolled = currentScrollTop > MINIMUM_SCROLL;

    setTimeout(() => {
      setTransparentHeader(isMinimumScrolled);
    }, TIMEOUT_DELAY);
  });

  const headerStyle = transparentHeader ? transparent : defaultHeader;

  return (
    <div css={headerStyle}>
      <HeaderTitle>{props.headerTitle ?? 'PokeQL'}</HeaderTitle>
    </div>
  );
}