import React, { Fragment } from 'react';
import { Link } from "react-router-dom";

/** @jsxImportSource @emotion/react */
import { jsx } from '@emotion/react';
import styled from '@emotion/styled';

import Colors from '../assets/themes/colors'

const Container = styled.div`
  position: fixed;
  bottom: 0;
  background-color: ${props => props.backgroundColor || 'white'};
  width: 100%;
  height: 50px;
  box-shadow: rgba(12,12,13,0.05) 0px -1px 2px 0px;
  display: flex;
  max-width: 500px;
  box-sizing: border-box;
`;

const StyledLink = styled(Link)`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  text-decoration: none;
`;

const NavText = styled.div`
  color: ${props => props.textColor || 'black'};
  font-size: 14px;
  font-family: 'Barlow Semi Condensed', sans-serif;
  font-weight: 400;
`;

export function TabNav(props) {
  return(
    <Container backgroundColor={Colors.keppel}>
      <StyledLink to={"/"}>
        <NavText textColor={Colors.twilight}>Pokemon List</NavText>
      </StyledLink>
      <StyledLink to={"/owned_pokemon"}>
        <NavText textColor={Colors.twilight}>Owned Pokemons</NavText>
      </StyledLink>
    </Container>
  )
}