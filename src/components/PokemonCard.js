import React from 'react';
import { Link } from "react-router-dom";

/** @jsxImportSource @emotion/react */
import { jsx, css } from '@emotion/react';
import styled from '@emotion/styled';

import Colors from '../assets/themes/colors'

const container = css({
  width: '30%',
  fontColor: 'black',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  textDecoration: 'none',
  border: '1px solid lightgrey',
  margin: '5px 5px',
  borderRadius: '5px',
  padding: '10px 0px',
  backgroundColor: Colors.twilight,
});

const PokemonName = styled.div`
  font-size: 16px;
  font-family: 'Barlow Semi Condensed', sans-serif;
  color: black;
  font-weight: bold;
  text-align: center;
  text-transform: uppercase;
`;

const EmptyDiv = styled.div`
  height: 15px;
`;

export function PokemonCard({ data, count }) {
  return (
    <Link css={container} to={"/pokemon/" + data.name}>
      <PokemonName>{data.nickname ?? data.name}</PokemonName>
      <img src={data.image} alt={data.name} />
      {count > 0 ? <PokemonName>Owned: {count} </PokemonName> : <EmptyDiv />}
    </Link>
  )
}