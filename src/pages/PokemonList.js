import React, { Fragment, useContext } from 'react';
import { useQuery } from '@apollo/client';
import { GET_POKEMON_LIST } from '../graphql/gql-pokemon';

/** @jsxImportSource @emotion/react */
import { jsx } from '@emotion/react';
import styled from '@emotion/styled';

import { OwnPokemonContext } from "../contexts/OwnPokemonContext";

import Colors from '../assets/themes/colors'

import { Header } from '../components/Header';
import { TabNav } from '../components/TabNav';
import {PokemonCard} from '../components/PokemonCard';

const Container = styled.div`
  display: flex;
  font-family: 'Barlow Semi Condensed', sans-serif;
  padding: 50px 10px;
  flex-wrap: wrap;
  justify-content: space-between;
  background-color: ${Colors.scandal};
`;

export function PokemonList(props) {
  const { state, dispatch } = useContext(OwnPokemonContext);

  const calculateOwnCount = (pokemon) => {

    let ownCount =  state.ownPokemons.filter(obj => {
      return obj.name === pokemon.name
    })

    return ownCount.length
  }
  
  const { loading, error, data } = useQuery(GET_POKEMON_LIST, {
    variables: {
      limit: 60, 
      offset: 0
    }
  })

  if (loading) return 'Loading...';
  if (error) return `Error! ${error.message}`;

  return (
    <Fragment>
      <Header />
      <Container>
        {data.pokemons.results.map((pokemon) => (
          <PokemonCard key={pokemon.id + pokemon.name} data={pokemon} count={calculateOwnCount(pokemon)}/>
        ))}
      </Container>
      <TabNav />
    </Fragment>
  )
}