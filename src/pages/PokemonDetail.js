import React, { Fragment, useState } from 'react';
import { useQuery } from '@apollo/client';
import { GET_POKEMON } from '../graphql/gql-pokemon';

/** @jsxImportSource @emotion/react */
import { jsx } from '@emotion/react';
import styled from '@emotion/styled';

import Colors from '../assets/themes/colors'

import { Header } from '../components/Header';
import { TabNav } from '../components/TabNav';
import { NotifModalComponent, CatchModalComponent } from '../components/Modal';
import pokeball from '../assets/images/pokeball.png';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  font-family: 'Barlow Semi Condensed', sans-serif;
  padding: 50px 10px;
  background-color: ${Colors.scandal};
`;

const ImageContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
`;

const PokemonImage = styled.img`
  width: 50%;
`;

const CatchContainer = styled.div`
  width: 50%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const CatchButton = styled.img`
  width: 30%;
  border-radius: 50%;
  &:hover {
    box-shadow: rgba(49, 53, 59, 0.5) 1px 3px 10px 1px;
  }
  &:focus {
    box-shadow: rgba(49, 53, 59, 0.5) 1px 3px 10px 1px;
  }
`;

const CatchText = styled.div`
  font-size: 14px;
  font-family: 'Barlow Semi Condensed', sans-serif;
  font-weight: bold;
  text-align: center;
`;

const Card = styled.div`
  background-color: white;
  border-radius: 4px;
  box-sizing: border-box;
  width: 100%;
  padding: 30px 20px;
  margin-bottom: 20px;
`;

const PokemonName = styled.div`
  font-size: 24px;
  font-family: 'Barlow Semi Condensed', sans-serif;
  color: black;
  font-weight: bold;
  text-align: center;
  margin: 0 0 15px 0;
  text-transform: uppercase;
`;

const PokemonAttrs = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  border-bottom: 1px solid lightgrey;
  padding-bottom: 20px;
  margin-bottom: 20px;
`;

const AttrContainer = styled.div`
  text-align: center;
`;

const AttrText = styled.div`
  font-size: 14px;
  font-family: 'Barlow Semi Condensed', sans-serif;
  color: lightgrey;
  font-weight: bold;
  text-transform: uppercase;
`;

const AttrValue = styled.div`
  font-size: 20px;
  font-family: 'Barlow Semi Condensed', sans-serif;
  color: black;
  margin-bottom: 5px;
`;

const TypeContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const TypeText = styled.div`
  font-size: 14px;
  font-family: 'Barlow Semi Condensed', sans-serif;
  color: lightgrey;
  font-weight: bold;
  text-align: center;
  text-transform: uppercase;
  & + div {
    margin-left: 5px;
  }
`;

const MoveContainer = styled.div`
  overflow: scroll;
  flex-wrap: wrap;
  display: flex;
`;

const MoveText = styled.div`
  font-size: 14px;
  font-family: 'Barlow Semi Condensed', sans-serif;
  color: black;
  font-weight: bold;
  width: 33%;
  text-align: center;
`;

function toUpper(str){
  return str
    .toLowerCase()
    .split(' ')
    .map(function(word) {
      return word[0].toUpperCase() + word.substr(1);
    })
    .join(' ');
}

export function PokemonDetail(props) {
  const [catchedPokemon, setCatchedPokemon] = useState('');
  const [showNotifModal, setNotifModal] = useState(false);
  const [showCatchModal, setCatchModal] = useState(false);
  const [modalMessage, setModalMessage] = useState('');

  const catchPokemon = (obj) => {
    let success = Math.random() < 0.5
    if(success){
      setCatchedPokemon(obj)
      setCatchModal(true)
    }
    else{
      setModalMessage('Failed! Please Try Again...')
      setNotifModal(true)
      setTimeout(() => setNotifModal(false), 3000)
    }
  }

  const pokemonSaved = () => {
    setCatchModal(false)
    setModalMessage('Pokemon added to owned pokemon list')
    setNotifModal(true)
    setTimeout(() => setNotifModal(false), 3000)
  }
  
  let queryParam = props.match.params;
  const { loading, error, data } = useQuery(GET_POKEMON, {
    variables: {
      name: queryParam.name,
    }
  })

  if (loading) return 'Loading...';
  if (error) return `Error! ${error.message}`;

  let pokemon = data.pokemon;

  return (
    <Fragment>
      <Header headerTitle={toUpper(pokemon.name)}/>
      <Container>
        <ImageContainer>
          <PokemonImage src={pokemon.sprites.front_default} alt={pokemon.name} />
          <CatchContainer>
            <CatchButton onClick={() => catchPokemon({ name: pokemon.name, nickname: '', image: pokemon.sprites.front_default })} src={pokeball} alt={'Pokeball'} />
            <CatchText>Catch!</CatchText>
          </CatchContainer>
        </ImageContainer>
        <Card>
          <PokemonName>{pokemon.name}</PokemonName>
          <PokemonAttrs>
            <AttrContainer>
              <AttrValue>{pokemon.weight}lbs</AttrValue>
              <AttrText>weight</AttrText>
            </AttrContainer>
            <TypeContainer>
              {pokemon.types.map((obj, typeIndex) => (
                <TypeText key={obj.type.name + typeIndex}>{obj.type.name}{typeIndex != pokemon.types.length-1 ? " /" : ""}</TypeText>
              ))}
            </TypeContainer>
            <AttrContainer>
              <AttrValue>{pokemon.height / 10}m</AttrValue>
              <AttrText>height</AttrText>
            </AttrContainer>
          </PokemonAttrs>
          <MoveContainer>
            {pokemon.moves.map((obj, moveIndex) => (
              <MoveText key={obj.move.name + moveIndex}>{toUpper(obj.move.name.replace(/-/g,' '))}</MoveText>
            ))}
          </MoveContainer>
        </Card>
        <NotifModalComponent message={modalMessage} show={showNotifModal} closeModal={setNotifModal} />
        <CatchModalComponent message={modalMessage} show={showCatchModal} pokemon={catchedPokemon} confirmHandler={pokemonSaved} />
      </Container>
      <TabNav />
    </Fragment>
  )
}