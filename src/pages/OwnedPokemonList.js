import React, { Fragment, useContext, useState } from 'react';

/** @jsxImportSource @emotion/react */
import { jsx } from '@emotion/react';
import styled from '@emotion/styled';

import { OwnPokemonContext } from "../contexts/OwnPokemonContext";
import Colors from '../assets/themes/colors'

import { Header } from '../components/Header';
import { TabNav } from '../components/TabNav';
import { NotifModalComponent, ReleaseModal } from '../components/Modal';

const Container = styled.div`
  display: flex;
  font-family: 'Barlow Semi Condensed', sans-serif;
  padding: 50px 10px;
  flex-direction: column;
  background-color: ${Colors.scandal};
  width: 100%;
`;

const PokemonRow = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  border: 1px solid ${Colors.mirage};
  border-radius: 8px;
  background-color: ${Colors.swansDown};
  margin: 10px 0px;
  justify-content: space-evenly;
  align-items: center;
`;

const PokemonName = styled.div`
  font-size: 16px;
  font-family: 'Barlow Semi Condensed', sans-serif;
  color: ${Colors.black};
  font-weight: bold;
  text-align: center;
  text-transform: uppercase;
`;

const ReleaseButton = styled.button`
  font-size: 16px;
  font-family: 'Barlow Semi Condensed', sans-serif;
  color: red;
  font-weight: bold;
  text-align: center;
  text-transform: uppercase;
  border: none;
  background-color: transparent;
  outline: none;
  cursor: pointer;
`;

export function OwnedPokemonList(props) {
  const { state, dispatch } = useContext(OwnPokemonContext);
  const [releasedPokemon, setReleasedPokemon] = useState('');
  const [releaseModal, setReleaseModal] = useState(false);
  
  const [notifModal, setNotifModal] = useState(false);
  const [modalMessage, setModalMessage] = useState('');

  const releasePokemon = (obj) => {
    setReleasedPokemon(obj)
    setReleaseModal(true)
  }

  const pokemonReleased = () => {
    setReleaseModal(false)
    setModalMessage('Pokemon Released!')
    setNotifModal(true)
    setTimeout(() => setNotifModal(false), 3000)
  }

  return (
    <Fragment>
      <Header />
      <Container>
        {
          state.ownPokemons.length <= 0 
          ? <div>No Data....</div>
          : state.ownPokemons.map((ownPokemon, index) => (
              <PokemonRow key={index}>
                <img src={ownPokemon.image} alt={ownPokemon.nickname || ownPokemon.name} />
                <PokemonName>{ownPokemon.nickname || ownPokemon.name}</PokemonName>
                <ReleaseButton onClick={() => releasePokemon(ownPokemon)}>Release</ReleaseButton>
              </PokemonRow>
            ))
        }
        <NotifModalComponent message={modalMessage} show={notifModal} closeModal={setNotifModal} />
        <ReleaseModal show={releaseModal} closeModal={setReleaseModal} pokemon={releasedPokemon} confirmHandler={pokemonReleased} />
      </Container>
      <TabNav />
    </Fragment>
  )
}