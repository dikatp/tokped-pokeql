import React, { createContext, useEffect, useReducer } from "react";

const initialState = {
  ownPokemons: [],
  lastId: 1,
};

const OwnPokemonContext = createContext();

let ownReducer = (state, action) => {
  switch(action.type){
    case "CATCH_POKEMON":
      return {
        ownPokemons: [...state.ownPokemons, action.payload.data],
        lastId: state.lastId + 1,
      }
    case "DELETE_POKEMON":
      let newData = state.ownPokemons.filter(obj => {
        return obj.id !== action.payload.data.id
      })

      return {
        ...state,
        ownPokemons: newData,
      }
    default:
      return state
  }
}

const localState = JSON.parse(localStorage.getItem("own_pokemons"));

function OwnPokemonProvider(props) {
  const [state, dispatch] = useReducer(ownReducer, localState || initialState);
  useEffect(() => {
    localStorage.setItem("own_pokemons", JSON.stringify(state));
  }, [state]);
  return (
    <OwnPokemonContext.Provider
      value={{
        state,
        dispatch,
      }}
    >
      {props.children}
    </OwnPokemonContext.Provider>
  );
}
export { OwnPokemonContext, OwnPokemonProvider };