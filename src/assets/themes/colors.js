const Colors = {
  mirage: '#17252A',
  paradiso: '#2B7A78',
  keppel: '#3AAFA9',
  swansDown: '#DEF2F1',
  scandal: '#C6F9EC',
  twilight: '#FEFFFF',
  black: '#000000',
}

export default {...Colors}